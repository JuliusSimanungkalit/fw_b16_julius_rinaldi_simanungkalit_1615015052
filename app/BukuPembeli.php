<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\BukuPembeli;
use App\Buku;
use App\Pembeli;

class BukuPembeli extends Model
{
	protected $table = 'buku_pembeli';
	protected $fillable = ['pembeli_id', 'buku_id',];
}
