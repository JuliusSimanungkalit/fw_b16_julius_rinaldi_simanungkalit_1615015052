<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penulis extends Model
{
    protected $table = 'penulis';//penamaan table harus plurar
    protected $fillable = [//bagian mana aja yang mau diisi
    	'nama','notlp','email','alamat',//koma diujung itu tidak wajib....
    ];
}
