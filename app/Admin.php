<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
	protected $table = 'admin'; 
    //protected $fillable = ['nama', 'notlp', 'email', 'alamat', 'pengguna_id',];

	public function Pengguna(){

		return $this->belongsTo(Pengguna::class);
	}
	public function getUsernameAttribute()
	{         
		return $this->pengguna->username; 
	} 
	
	public function getPasswordAttribute()
	{         
		return $this->pengguna->password; 
	}
}
