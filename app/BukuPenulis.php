<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BukuPenulis extends Model
{
    protected $table = 'buku_penulis';//penamaan table harus plurar
    protected $fillable = [//bagian mana aja yang mau diisi
    	'penulis_id','buku_id',//koma diujung itu tidak wajib....
    ];
}
