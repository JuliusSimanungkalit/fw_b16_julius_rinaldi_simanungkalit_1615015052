<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests; 
use App\BukuPembeli;
use App\Buku; 
use App\Pembeli;
use App\Penulis;

class ControllerBukuPembeli extends Controller
{
    public function coba()
	{   
		$buku_pembeli = Buku::find(1)->buku_pembeli()->where('judul','Harry Potter')->first();
		dd($buku_pembeli);  
	}    
	public function awal(){
    	$buku_pembeli = BukuPembeli::all();
    	return view('pesanbuku.app', compact('buku_pembeli')); //.app itu nama folder
}
	// public function lihat()
	// {   
	// 	return view("buku.menu");  
	// }    
	public function edit($id)
	{   
		$buyer = Pembeli::all('nama','id')->pluck('nama','id');   
		$book = Buku::all(['judul','id'])->pluck('judul','id');   
		$buku_pembeli = BukuPembeli::find($id);   
		return view('pesanbuku.edit', compact('buyer'), compact('book'))->with(array('buku_pembeli'=>$buku_pembeli));  
	}    
	public function update($id, Request $input)
	{   
		$buku_pembeli = BukuPembeli::find($id);   
		$id2 = $buku_pembeli->pembeli->first()->pivot->id; 
		$id3 = $buku_pembeli->buku->first()->pivot->id;  
		$buku_pembeli->save();
		$buku_pembeli->pembeli()->newPivotStatement()->where('id', $id2)->update(['pembeli_id' => $input->pembeli]);   
		$buku_pembeli->pembeli()->attach($input->pembeli);   
		$buku_pembeli->buku()->newPivotStatement()->where('id', $id3)->update(['buku_id' => $input->buku]);   
		$buku_pembeli->buku()->attach($input->buku);
		return redirect('pesanbuku');  
	}       
	public function tambah()
	{   
		$buyer = Pembeli::all('nama','id')->pluck('nama','id');   
		$book = Buku::all(['judul','id'])->pluck('judul','id');   
		return view('pesanbuku.tambah', compact('buyer'), compact('book'));  
	}    
	public function simpan(Request $input)
	{      
		// $this->validate($input, array
		// 	(
		// 		'judul'=>'required', 
		// 		'kategori'=>'required|integer',
		// 		'penerbit'=>'required',
		// 		'tanggal' => 'required'
		// 	));      
		$buku_pembeli = new BukuPembeli();   
		$buku_pembeli->pembeli_id = $input->buyer; 
		$buku_pembeli->buku_id = $input->book;   
		$buku_pembeli->save();   
		$buku_pembeli->pembeli()->attach($input->pembeli_id);
		$buku_pembeli->buku()->attach($input->buku_id);   
		return redirect('pesanbuku')->with(['status'=>$status]);
	}    
	public function hapus($id)
	{   
		$buku_pembeli = BukuPembeli::find($id);   
		$buku_pembeli->buku()->detach();   
		$buku_pembeli->delete();   
		return redirect('pesanbuku');   
	} 
}
