<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin;
use App\Pengguna;

class ControllerAdmin extends Controller
{
	public function awal(){
		$admin = Admin::all();
		return view('admin.app',compact('admin'));
	}

	public function tambah(){
		return view("admin.tambah");
	}

	public function simpan(Request $input){
		$this->validate($input, array
			(
				'nama'=>'required', 
				'notlp'=>'required|integer',
				'email'=>'required',
				'alamat' => 'required',
				'username' => 'required|min:4',
				'password' => 'required|max:20'
			));
		$pengguna = new Pengguna();
		$pengguna->username = $input->username;
		$pengguna->password = $input->password;
		$pengguna->level = "admin";
		$pengguna->save();

		$admin = new Admin();
		$admin->nama = $input->nama;
		$admin->notlp = $input->notlp;
		$admin->email = $input->email;
		$admin->alamat = $input->alamat;
		$admin->pengguna_id=$pengguna->id;
		$status=$admin->save();
		return redirect('admin')->with(['status'=>$status]);
	}

	public function edit($id){
		$admin = Admin::find($id);
		return view('admin.edit')-> with(array('admin'=>$admin));
	}

	public function update($id, Request $input){
		$admin = Admin::find($id);
		$admin->nama = $input->nama;
		$admin->notlp = $input->notlp;
		$admin->email = $input->email;
		$admin->alamat = $input->alamat;
		
		$pengguna = Pengguna::find($admin->pengguna_id);  
		$pengguna->username = $input->username;   
		$pengguna->password = $input->password;   
		$pengguna->level = "admin";   
		$pengguna->save();
		$status=$admin->save();
		return redirect('admin')->with(['status'=>$status]);
	}

	public function hapus($id){
		$admin = Admin::find($id);
		$admin->delete();
		return redirect('admin');
	}

}
