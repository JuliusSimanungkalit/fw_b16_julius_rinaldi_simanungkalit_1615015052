<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Penulis;

class ControllerPenulis extends Controller
{
	public function awal()
	{
		$penulis = Penulis::all();
    	return view('penulis.app',compact("penulis"));//dibungkus oleh compact dan ditampilkan oleh app
    }//penulis itu nama folder .(titik itu sebagai pengganti garis miring) app itu nama file

    public function tambah(){
    	return view('penulis.tambah');
    }

    public function simpan(Request $input){
    	$this->validate($input, array
          (
            'nama'=>'required', 
            'notlp'=>'required|integer',
            'email'=>'required',
            'alamat' => 'required',
        ));

        $penulis = new Penulis();
        $penulis->nama = $input->nama;
        $penulis->notlp = $input->notlp;
        $penulis->email = $input->email;
        $penulis->alamat = $input->alamat;
        $status=$penulis->save() ? 'Data Has Been Saved' : 'There is some error on your code';
        return redirect('penulis')->with(['status'=>$status]);
    }

    public function edit($id){
    	$penulis= Penulis::find($id);
    	return view('penulis.edit')->with(array('penulis' => $penulis));
    }

    public function update($id, Request $input){
    	$penulis = Penulis::find($id);
    	$penulis->nama = $input->nama;
    	$penulis->notlp = $input->notlp;
    	$penulis->email = $input->email;
    	$penulis->alamat = $input->alamat;
    	$status=$penulis->save() ? 'Data Has Been Saved' : 'There is some error on your code';
    	return redirect("penulis")->with(['status'=>$status]);
    }

    public function hapus($id){
    	$penulis = Penulis::find($id);
    	$penulis->delete();
    	return redirect('penulis');
    }

}
