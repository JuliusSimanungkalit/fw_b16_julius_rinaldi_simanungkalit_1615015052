<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table = 'kategori';//penamaan table harus plurar
    protected $fillable = [//bagian mana aja yang mau diisi
    	'deskripsi',//koma diujung itu tidak wajib....
    ];
}
