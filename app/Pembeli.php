<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pembeli extends Model
{
    protected $table = 'pembeli';//penamaan table harus plurar
    // protected $fillable = [//bagian mana aja yang mau diisi
    // 	'nama','notlp','email','alamat','pengguna_id',//koma diujung itu tidak wajib....
    // ];
    public function Pengguna(){
    	return $this->belongsTo(Pengguna::class);
    }

    Public function getUsernameAttribute(){
    	Return $this->pengguna->username;
    }
    Public function getPasswordAttribute(){
    	Return $this->pengguna->password;
    }
}
