<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class pengguna extends Authenticatable
{
    //
    protected $table = 'pengguna';//penamaan table harus plurar
    protected $fillable = [//bagian mana aja yang mau diisi
    	'username','password','level',//koma diujung itu tidak wajib....
    ];
    protected $hidden = [//supaya tidak bisa terlihat di cookie, remember token itu
    	'password','remember_token',
    ];
    //bisa membuat sekaligus mmigration
    //untuk pembuatan model harus huruf besar

    
}
