<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


// Route::get('/', function () {
//     return 'Hello Word';
// });

// Route::get('pengguna/{pengguna}', function ($pengguna) {
//     return "Hello Word dari pengguna $pengguna";
// });
Route::get('home','PenggunaController@simpan');
Route::get('simpanall','PenggunaController@awal2');

Route::get('pengguna','PenggunaController@awal');

Route::get('pengguna/tambah','PenggunaController@tambah');

Route::get('/','AuthController@index');
Route::get('/login','AuthController@formLogin');
Route::post('/login','AuthController@proses');
Route::get('/logout','AuthController@logout');

Route::get('/tambah/penulis',"ControllerPenulis@tambah");
Route::post('/penulis/simpan',"ControllerPenulis@simpan");
Route::get('/penulis',"ControllerPenulis@awal");
Route::get('/penulis/edit/{penulis}',"ControllerPenulis@edit");
Route::post('/penulis/update/{penulis}',"ControllerPenulis@update");
Route::get('/penulis/hapus/{penulis}',"ControllerPenulis@hapus");

Route::get('/tambah/buku',"ControllerBuku@tambah");
Route::post('/buku/simpan',"ControllerBuku@simpan");
Route::get('/buku',"ControllerBuku@awal");
Route::get('/tambah',"ControllerBuku@lihat");

Route::get('/tambah/kategori',"ControllerKategori@tambah");
Route::get('/kategori',"ControllerKategori@awal");
Route::post('/kategori/simpan',"ControllerKategori@simpan");
Route::get('/kategori/edit/{kategori}',"ControllerKategori@edit");
Route::post('/kategori/update/{kategori}',"ControllerKategori@update");
Route::get('/kategori/hapus/{kategori}',"ControllerKategori@hapus");


Route::get('/tambah/pembeli',"ControllerPembeli@tambah");
//Route::post('/pembeli/signeurlpaiement(clent, data)mpan',"ControllerPembeli@simpan");
Route::get('/pembeli',"ControllerPembeli@awal");
Route::get('/pembeli/edit/{pembeli}',"ControllerPembeli@edit");
Route::post('/pembeli/update/{pembeli}',"ControllerPembeli@update");
Route::get('/pembeli/hapus/{pembeli}',"ControllerPembeli@hapus");
Route::post('/pembeli/simpan',"ControllerPembeli@simpan");


Route::get('/admin',"ControllerAdmin@awal");
Route::get('/tambah/admin',"ControllerAdmin@tambah");
Route::post('/admin/simpan',"ControllerAdmin@simpan");
Route::get('/admin/lihat',"ControllerAdmin@lihat");
Route::get('/admin/edit/{admin}',"ControllerAdmin@edit");
Route::post('/admin/update/{admin}',"ControllerAdmin@update");
Route::get('/admin/hapus/{admin}',"ControllerAdmin@hapus");

Route::get('/buku_pembeli', "BukuPembeliController@awal");
Route::get('/tambah/buku_pembeli', "BukuPembeliController@tambah");
Route::post('/buku_pembeli/simpan',"BukuPembeliController@simpan");
Route::get('/buku_pembeli/edit/{buku_pembeli}',"BukuPembeliController@edit");
Route::post('/buku_pembeli/update/{buku_pembeli}',"BukuPembeliController@update");
Route::get('/buku_pembeli/hapus/{buku_pembeli}',"BukuPembeliController@hapus");