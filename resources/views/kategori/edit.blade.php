@extends('master')
<div class="panel panel-primary">
{!! Form::model($kategori,['url'=>'kategori/update/'.$kategori->id,'class'=>'form-horizontal']) !!}
@include('kategori.form')
<div style="width:100%;text-align:center;">
<button class="btn btn-primary"><i class="fa fa-save"></i>
Simpan</button>
<input type="button" value="Reset" class="btn btn-danger" onClick="window.location.reload()"/>
</div>
{!! Form::close() !!}
</div>