@extends('master')
@section('content')
{{ $status or ' ' }}
<div class="panel panel-info">
<div class="panel-heading">
Data kategori 
<div class="pull-right">
Tambah Data <a href="{{ url('tambah/kategori')}}"><img src="{{ asset('add.icon') }}" height="20"></img></a>
</div>
</div>
<div class="panel-body">
<table class="table">
<tr>
<td>ID</td>
<td>Deskripsi</td>

</tr>
@foreach($kategori as $Kategori)
<tr>
<td >{{ $Kategori->id }}</td>	
<td >{{ $Kategori->deskripsi }}</td>

<td >
<a href="{{url('kategori/edit/'.$Kategori->id)}}"><img src="{{ asset('edit.png') }}" height="20"></img></a>
<a href="{{url('kategori/hapus/'.$Kategori->id)}}"><img src="{{ asset('delete.png') }}" height="20"></img></a>
</td>
</tr>
@endforeach
</table>
</div>
</div>
@endsection