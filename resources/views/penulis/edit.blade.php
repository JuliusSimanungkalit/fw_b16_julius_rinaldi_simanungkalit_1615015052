@extends('master')
<div class="panel panel-primary">
	{!! Form::model($penulis,['url'=>'penulis/update/'.$penulis->id,'class'=>'form-horizontal']) !!}
	@include('penulis.form')
	<div style="width:100%;text-align:center;">
		<button class="btn btn-primary"><i class="fa fasave"></i>
			Simpan</button>
			<input type="button" value="Reset" class="btn btndanger" onClick="window.location.reload()"/>
		</div>
		{!! Form::close() !!}
	</div>