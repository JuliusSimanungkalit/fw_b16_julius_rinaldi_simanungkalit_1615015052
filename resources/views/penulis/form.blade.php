<div class="form-group">
	@if ($errors->any())
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
	@endif
</div>
<div class="form-group">
	<label class="col-sm-2">Nama</label>
	<div class="col-sm-9">
		{!! Form::text('nama',null,['class'=>'formcontrol','placeholder'=>"Nama"]) !!}
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2">No Telp</label>
	<div class="col-sm-9">
		{!! Form::text('notlp',null,['class'=>'formcontrol','placeholder'=>"No Telepon"]) !!}
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2">Email</label>
	<div class="col-sm-9">
		{!! Form::text('email',null,['class'=>'formcontrol','placeholder'=>"E-Mail"]) !!}
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2">Alamat</label>
	<div class="col-sm-9">
		{!! Form::text('alamat',null,['class'=>'formcontrol','placeholder'=>"Alamat"]) !!}
	</div>
</div>