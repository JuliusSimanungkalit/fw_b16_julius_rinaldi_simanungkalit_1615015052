<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Login</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
	<link rel="stylesheet" type="text/css" href="{{
	asset('font-awesome/css/font-awesome.min.css') }}">
</head>
<body>
	<nav class="navbar navbar-default navbar-static-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar">
				<span class="sr-only"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a href="{{url('/')}}" class="navbar-brand">Laravel</a>
		</div>
	</div>
</nav>
<div class="clearfix"></div>
<div class="container">
	<div class="row">
		@if(count($errors) > 0 )
		<div class="alert alert-danger">
			<ul>
				@foreach($errors->all() as $error)
				<li>{{$error}}</li>
				@endforeach
			</ul>
		</div>
		@endif
		<div class="panel panel-info">
			<div class="panel-heading"><strong>Login</strong></div>
			<div class="panel-body">
				{{ Form::open(['url' => 'login','method' => 'POST']) }}
				<div class="form-group">
					<label class="control-label">Username</label>
					{{ Form::text('username',null,['class' =>
					'form-control','placeholder' => 'masukkan username'])}}
				</div>
				<div class="form-group">
					<label>Password</label>
					{{ Form::password('password',['class' =>
					'form-control','placeholder' => 'masukkan password'])}}
				</div>
				<div class="form-group">
					<button class="btn
					btn-success">Login</button>
				</div>
			</body>
			{{ Form::close() }}
		</div>
	</div>
</div>
</div>
<nav class="navbar navbar-default navbar-fixed-bottom">
	<footer class="container">
		<p align="center">Created by Aslab Framework 2018</p>
	</footer>
</nav>
<script type="text/javascript" src="{{ asset('js/app.js')}}"></script>
</body>
</html>