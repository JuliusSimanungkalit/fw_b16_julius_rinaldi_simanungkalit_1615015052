<div class="form-group">
	<center>
		<label class="col-sm-2 control-label">Data Buku</label>
		@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif
		<div class="col-sm-5">
			{!! Form::text('judul',null,['class'=>'formcontrol','placeholder'=>"Judul"]) !!}
		</div>
		<div class="col-sm-5">
			{!! Form::Label('penulis', 'Pilih Penulis') !!}
			{!! Form::select('penulis', $author, null, ['class' => 'form-control']) !!}
		</div>
		<div class="col-sm-5">
			{!! Form::Label('kategori', 'Pilih Kategori') !!}
			{!! Form::select('kategori', $categories, null, ['class' =>'form-control']) !!}
		</div>
		<div class="col-sm-5">
			{!! Form::Label('penerbit', 'Penerbit') !!}
			{!! Form::text('penerbit',null,['class'=>'formcontrol','placeholder'=>"Penerbit"]) !!}
		</div>
		<div class="col-sm-5">
			{!! Form::Label('tanggal', 'Tanggal Rilis') !!}
			{!! Form::text('tanggal',null,['class'=>'formcontrol','placeholder'=>"Tanggal Rilis"]) !!}
		</div>
	</center>
</div>