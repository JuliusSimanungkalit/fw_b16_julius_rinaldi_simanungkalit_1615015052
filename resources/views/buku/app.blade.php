@extends('master')
@section('content')
{{ $status or ' ' }}
<div class="panel panel-info">
	<div class="panel-heading">
		Data Buku
		<div class="pull-right">
			Tambah Data <a href="{{url('tambah/buku')}}"><img src="{{ asset('add.ico') }}" height="20"></img></a>
		</div>
	</div>
	<div class="panel-body">
		<table class="table">
			<tr>
				<td>Judul</td>
				<td>Kategori</td>
				<td>Penerbit</td>
				<td>Penulis</td>
				<td>Aksi</td>
			</tr>
			@foreach( $datac as $buku)  
			<tr>
				<td >{{ $buku->judul }}</td>
				<td >{{ $buku->kategori->deskripsi or 'kosong'}}</td>
				<td >{{ $buku->penerbit }}</td>
				<td >{{ $buku->penulis->nama or 'kosong'}}</td>
				<td >
					<a href="{{url('buku/edit/'.$buku->id)}}" class= "btn btn-warning btn-xs">Edit</a>
					<a href="{{url('buku/hapus/'.$buku->id)}}"class= "btn btn-danger btn-xs">Hapus</a>
				</td>
			</tr>
			@endforeach
		</table>
	</div>
</div>
@endsection