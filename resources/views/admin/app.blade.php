@extends('master')
@section('content')
{{ $status or ' ' }}
<div class="panel panel-info">
	<div class="panel-heading">
		Data Admin
		<div class="pull-right">
			<a href="{{ url('tambah/admin')}}" class="btn btn-success btn-xs">Tambah Data</a>
		</div>
	</div>
	<div class="panel-body">
		<table class="table">
				<tr>
					<td>Nama</td>
					<td>No Telepon</td>
					<td>Email</td>
					<td>Alamat</td>
					<td>Username</td>
					<td>Aksi</td>
				</tr>
				@foreach($admin as $Admin)
					
				<tr>
					<td >{{ $Admin->nama }}</td>
					<td >{{ $Admin->notlp}}</td>
					<td >{{ $Admin->email }}</td>
					<td >{{ $Admin->alamat}}</td>
					<td >{{ $Admin->username}}</td>
					<td >
					
					<a href="{{url('admin/edit/'.$Admin->id)}}" class="btn btn-primary btn-xs">Edit</a>
					<a href="{{url('admin/hapus/'.$Admin->id)}}" class="btn btn-danger btn-xs">Hapus</a>
					</td>
				</tr>
				@endforeach
			</table>
	</div>
</div>
@endsection
