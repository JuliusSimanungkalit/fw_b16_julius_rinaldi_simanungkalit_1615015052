<div class="form-group">
	@if ($errors->any())
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
	@endif 
</div>

<div class="form-group">
	<label class="col-sm-2">Nama Pembeli</label>
	<div class="col-sm-9">   {!! Form::Label('buyer') !!}
		{!! Form::select('buyer', $buyer, null, ['class'=>'formcontrol']) !!} 
	</div>  
</div>
<div class="form-group">
	<label class="col-sm-2">Judul Buku</label>
	<div class="col-sm-9">   {!! Form::Label('book') !!}
		{!! Form::select('book', $book, null, ['class'=>'formcontrol']) !!} 
	</div>  
</div>
</center>  
</div>
