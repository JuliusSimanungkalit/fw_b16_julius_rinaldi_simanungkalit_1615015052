@extends('master') 
@section('content')  
{{ $status or ' ' }}  
<div class="panel panel-default">   
	<div class="panel-heading">    
		<strong>Data Pemesanan Buku</strong>    
		<div class="pull-right">   Tambah Data <a href="{{url('tambah/buku_pembeli')}}"><img src="{{ asset('add.ico') }}" height="20"></img></a>    
		</div>    
		<div class="penel-body">     
			<table class="table">            
				<tr>       
					<td> Nama Pembeli  </td>       
					<td> Judul Buku </td>       
					<td> Aksi  </td>      
				</tr>      
				@foreach($buku_pembeli as $buku_pembeli)         
				<tr>   
					<td>{{ $buku_pembeli->pembeli }}</td>   
					<td>{{ $buku_pembeli->buku->judul or 'kosong'}}</td>     
					<td>    
						<a href="{{url('buku_pembeli/edit/'.$buku_pembeli->id)}}"><img src="{{ asset('edit.png') }}" height="20"></img></a>                          
						<a href="{{url('buku_pembeli/hapus/'.$buku_pembeli->id)}}"><img src="{{ asset('delete.png') }}" height="20"></img></a>   
					</td>  
				</tr>      
				@endforeach     
			</table>    
		</div>   
	</div>  
</div>                    
@endsection 